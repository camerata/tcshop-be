from categories import views

from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'', views.SectionViewSet, base_name="sections")
urlpatterns = router.urls

from django.conf import settings
from django.db import models

from tcshop import safe_invert
from tcshop.models import NameDescriptionMixin, PriorityMixin, VerboseMixin, ChoiceEnum


class Section(NameDescriptionMixin):
    verbose_name = models.CharField(max_length=256)

    @property
    def categories(self):
        return self.all_categories.filter(parent=None)


class Category(NameDescriptionMixin, VerboseMixin):
    is_parent = models.BooleanField(default=False)
    is_comparable = models.BooleanField(default=False)
    parent = models.ForeignKey('self', related_name='children', on_delete=models.CASCADE, null=True)
    section = models.ForeignKey(Section, related_name="all_categories", on_delete=models.CASCADE)

    SYNONYMS = {
        "Принтер": "Принтеры и МФУ",
        "МФУ": "Принтеры и МФУ",
    }
    MAPPINGS = {}

    for v in settings.CATEGORIES["sections"].values():
        MAPPINGS.update(v["categories"])

    @classmethod
    def get_by_name(cls, name):
        if name in cls.SYNONYMS:
            query = cls.SYNONYMS[name]
        else:
            query = name
        return cls.objects.get(verbose_name__iexact=query)

    @classmethod
    def get_by_identifier(cls, name):
        return cls.objects.get(name__exact=name)

    def __str__(self):
        return self.name


class Imprint(models.Model):
    category = models.OneToOneField(Category, related_name="imprint", on_delete=models.CASCADE)
    created_at = models.DateField(auto_now_add=True)


class CharacteristicsGroup(NameDescriptionMixin, VerboseMixin, PriorityMixin):
    imprint = models.ForeignKey(Imprint, related_name="characteristics_group", on_delete=models.CASCADE)


class CharacteristicImprint(NameDescriptionMixin, PriorityMixin, VerboseMixin):
    class Types(ChoiceEnum):
        BOOL = 'bool'
        VECTOR = 'vector'
        SCALAR = 'scalar'
        ANY = 'any'

    group = models.ForeignKey(CharacteristicsGroup, related_name="characteristics", on_delete=models.CASCADE)
    type = models.CharField(max_length=64, choices=Types.choices(), default=Types.ANY)


class RangeConstraint(models.Model):
    range_characteristic = models.OneToOneField(CharacteristicImprint, related_name="range_constraint", on_delete=models.CASCADE)
    minimum = models.IntegerField(null=True)
    maximum = models.IntegerField(null=True)
    measure_unit = models.CharField(max_length=64, null=True)


class VectorConstraint(models.Model):
    vector_characteristic = models.OneToOneField(CharacteristicImprint, related_name="vector_constraint", on_delete=models.CASCADE)


class VectorConstraintOption(models.Model):
    constraint = models.ForeignKey(VectorConstraint, related_name="options", on_delete=models.CASCADE)
    option = models.CharField(max_length=256)
    order = models.SmallIntegerField()


# Proxy Models

class AnyCharacteristicType(CharacteristicImprint):
    class Meta:
        proxy = True


class BoolCharacteristicType(CharacteristicImprint):
    class Meta:
        proxy = True


class ScalarCharacteristicType(CharacteristicImprint):
    class Meta:
        proxy = True


class VectorCharacteristicType(CharacteristicImprint):
    class Meta:
        proxy = True

from commerce.permissions import ReadOnly
from categories.serializers import SectionSerializer
from categories.models import Section
from rest_framework import permissions, viewsets


class SectionViewSet(viewsets.ModelViewSet):
    queryset = Section.objects.all()
    serializer_class = SectionSerializer
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        ReadOnly,
    )

    class Meta:
        ordering = ('id',)


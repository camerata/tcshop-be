import os
import yaml
import math
import yamlordereddictloader

from django.core.management.base import BaseCommand, CommandError
from django.db.transaction import atomic
from django.conf import settings

from categories.models import (
    Category,
    Section,
    CharacteristicsGroup,
    Imprint,
    CharacteristicImprint,
    RangeConstraint,
    VectorConstraint,
    VectorConstraintOption
)


class Command(BaseCommand):
    help = 'Restores all categories to their usual state'

    IMPRINTS_DIR = os.path.join(
        settings.BASE_DIR,
        "categories",
        "imprints"
    )

    def _delete_previous_records(self):
        Section.objects.all().delete()
        self.stdout.write("Previous records successfully deleted")

    def _create_category(self, section, name, data, parent=None, cur_depth=0):
        category_is_parent = "children" in data
        new_category = Category.objects.create(
            name=name,
            verbose_name=data["verbose_name"],
            section=section,
            is_comparable=data.get("comparable", False),
            is_parent=category_is_parent,
            parent=parent
        )
        self.stdout.write(("\t" * (cur_depth + 1)) + "+ created category `{}`".format(name))
        if category_is_parent:
            for category_name, category_data in data["children"].items():
                self._create_category(
                    section,
                    category_name,
                    category_data,
                    parent=new_category,
                    cur_depth=cur_depth + 1
                )

    def _restore_category_tree(self):
        for section_name, section in settings.CATEGORIES["sections"].items():
            new_section = Section.objects.create(
                name=section_name,
                verbose_name=section["verbose_name"],
            )
            self.stdout.write("Populating categories")
            for category_name, category_data in section["categories"].items():
                self._create_category(
                    new_section,
                    category_name,
                    category_data,
                )

    def _create_characteristic(self, group, name, data, priority=0):
        char_type = data["type"]
        characteristic = CharacteristicImprint.objects.create(
            name=name,
            verbose_name=data["verbose_name"],
            description=data.get("desc", None),
            group=group,
            type=char_type,
            priority=priority
        )

        self.stdout.write("\t\t+ inserted characteristic `{}` of type {}".format(name, char_type))

        if char_type == "scalar":
            _min, _max = data["range"]

            RangeConstraint.objects.create(
                minimum=_min if _min != "-Inf" and _min != "+Inf" else None,
                maximum=_max if _max != "+Inf" and _max != "-Inf" else None,
                measure_unit=data.get("measure_unit", None),
                range_characteristic=characteristic
            )
        elif char_type == "vector":
            values = data["values"]
            constraint = VectorConstraint.objects.create(
                vector_characteristic=characteristic
            )
            for index, option in enumerate(values):
                if isinstance(option, list):
                    for sub_option in option:
                        VectorConstraintOption.objects.create(
                            option=sub_option,
                            order=index,
                            constraint=constraint
                        )

                else:
                    VectorConstraintOption.objects.create(
                        option=option,
                        order=index,
                        constraint=constraint
                    )

    def _restore_imprint_from_file(self, filename):
        name, data = yaml.load(
            open(os.path.join(self.IMPRINTS_DIR, filename)),
            Loader=yamlordereddictloader.Loader
        ).popitem()
        self.stdout.write("Opened: {}".format(name))
        category = Category.get_by_identifier(name)
        imprint = Imprint.objects.create(
            category=category,
        )

        self.stdout.write("Creating imprint for category `{}`".format(name))

        for group_index, (group_name, group_data) in enumerate(data["groups"].items()):
            new_group = CharacteristicsGroup.objects.create(
                name=group_name,
                verbose_name=group_data["verbose_name"],
                description=group_data.get("desc", None),
                imprint=imprint,
                priority=group_index
            )

            self.stdout.write("\t+ inserted group `{}`".format(group_name))

            for char_index, (char_name, char_data) in enumerate(group_data["items"].items()):
                self._create_characteristic(new_group, char_name, char_data, priority=char_index)

    def handle(self, *args, **options):
        with atomic():
            self._delete_previous_records()

            self._restore_category_tree()
            filenames = os.listdir(self.IMPRINTS_DIR)
            self.stdout.write("Following files are about to be processed: {}".format(", ".join(filenames)))
            for filename in filenames:
                self._restore_imprint_from_file(filename)

        self.stdout.write(self.style.SUCCESS('All categories restored'))

from rest_framework import serializers

from categories.models import Section, Category, Imprint, CharacteristicsGroup, CharacteristicImprint

from rest_framework_recursive.fields import RecursiveField


class ConstraintSerializerField(serializers.Field):
    def to_representation(self, obj):
        if obj.type == CharacteristicImprint.Types.SCALAR.value:
            return {
                "min": obj.range_constraint.minimum,
                "max": obj.range_constraint.maximum,
                "measure_unit": obj.range_constraint.measure_unit,
            }
        elif obj.type == CharacteristicImprint.Types.VECTOR.value:
            return {
                "values": [{
                    "option": option.option,
                    "order": option.order
                } for option in obj.vector_constraint.options.all()]
            }

    def to_internal_value(self, data):
        raise NotImplemented()


class CharacteristicImprintSerializer(serializers.ModelSerializer):
    constraints = ConstraintSerializerField(source='*')

    class Meta:
        model = CharacteristicImprint
        fields = ("name", "verbose_name", "priority", "type", "constraints")
        ordering = ("priority",)


class CharacteristicGroupSerializer(serializers.ModelSerializer):
    characteristics = CharacteristicImprintSerializer(many=True)

    class Meta:
        model = CharacteristicsGroup
        fields = ("characteristics", "name", "verbose_name", "priority")
        ordering = ("priority", )


class ImprintSerializer(serializers.ModelSerializer):
    characteristics_group = CharacteristicGroupSerializer(many=True)

    class Meta:
        model = Imprint
        fields = ("characteristics_group", )
        ordering = ("priority",)


class CategorySerializer(serializers.ModelSerializer):
    children = RecursiveField(required=False, allow_null=True, many=True)
    imprint = ImprintSerializer()

    class Meta:
        model = Category
        fields = ("id", "name", "verbose_name", "is_comparable", "is_parent", "children", "imprint")
        ordering = ("id", )


class SectionSerializer(serializers.ModelSerializer):
    categories = CategorySerializer(many=True)

    class Meta:
        model = Section
        fields = ('id', 'name', 'verbose_name', 'description', "categories")
        ordering = ("id",)

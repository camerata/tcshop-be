from django.db import models
from django.contrib.auth.models import AbstractUser
from enum import Enum


class ChoiceEnum(Enum):
    @classmethod
    def choices(cls):
        return tuple((x.name, x.value) for x in cls)


class User(AbstractUser):
    class Roles(ChoiceEnum):
        ADMIN = "Admin"
        MODERATOR = "Moderator"
        CUSTOMER = "Customer"

    phone = models.CharField(max_length=20)
    role = models.CharField(max_length=64, choices=Roles.choices(), default=Roles.CUSTOMER)


class Address(models.Model):
    city = models.CharField(max_length=20)
    street = models.CharField(max_length=20)
    house = models.SmallIntegerField()
    user = models.ForeignKey(User, related_name="addresses", on_delete=models.CASCADE)


class NameDescriptionMixin(models.Model):
    """
    Defines `name` and `description` properties on a model
    """
    name = models.CharField(max_length=256)
    description = models.TextField(null=True)

    class Meta:
        abstract = True


class OrderManager(models.Manager):
    def all(self):
        return self.get_queryset().order_by('priority')


class PriorityMixin(models.Model):
    """
    Defines `priority` property on a model to always return QuerySet sorted
    """
    priority = models.SmallIntegerField()
    objects = OrderManager()

    class Meta:
        ordering = ('priority',)
        abstract = True


class VerboseMixin(models.Model):
    """
    Defines `priority` property on a model to always return QuerySet sorted
    """
    verbose_name = models.CharField(max_length=256)

    class Meta:
        abstract = True

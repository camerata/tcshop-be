def safe_invert(mapping):
    """
    Return flattened dict of columns keycodes corresponding to verbose names
    :param mapping: dict
    :return: dict
    """
    result = {}

    for key, value in mapping.items():
        if isinstance(value, list) or isinstance(value, set):
            for entry in iter(value):
                result[entry] = key
        elif isinstance(value, dict):
            inverted_mapping = safe_invert(value)
            result.update(inverted_mapping)
        else:
            result[value] = key

    return result
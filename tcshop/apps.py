from django.apps import AppConfig


class TcShopConfig(AppConfig):
    name = 'tcshop'

from rest_framework import permissions


class ReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow read requests
    """

    def has_object_permission(self, request, view, obj):
        return request.method in permissions.SAFE_METHODS


class IsAdminOrModerator(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        return request.user.role in ("Admin", "Moderator")


class AllowAnonymous(permissions.BasePermission):
    """
    Custom permission to allow all requests
    """

    def has_object_permission(self, request, view, obj):
        return True

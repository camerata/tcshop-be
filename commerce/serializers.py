from rest_framework import serializers

from commerce.models import Product, Order, PurchasedProduct


class CharacteristicField(serializers.Field):
    def to_representation(self, obj):
        return {
            characteristic.name: characteristic.value
            for characteristic in obj.characteristics
        }

    def to_internal_value(self, data):
        raise NotImplemented()


class ProductItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = PurchasedProduct
        fields = (
            "price",
            "name"
        )


class OrderSerializer(serializers.ModelSerializer):
    items = ProductItemSerializer(many=True)

    class Meta:
        model = Order
        fields = (
            "id",
            "number",
            "total",
            "created_at",
            "open_id",
            "status",
            "items"
        )


class ProductSerializer(serializers.ModelSerializer):
    characteristics = CharacteristicField(source='*')
    category = serializers.CharField()

    class Meta:
        model = Product
        fields = (
            'id',
            'name',
            'description',
            'price',
            'vendor_code',
            'presence',
            'photo_path',
            'type',
            'characteristics',
            'category'
        )

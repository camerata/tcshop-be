from commerce import views

from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'products', views.ProductViewSet, base_name="products")
router.register(r'orders', views.OrderViewSet, base_name="orders")
urlpatterns = router.urls

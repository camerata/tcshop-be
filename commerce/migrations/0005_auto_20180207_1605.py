# Generated by Django 2.0.1 on 2018-02-07 16:05
import uuid
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('commerce', '0004_auto_20180207_0739'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='number',
            field=models.IntegerField(default=0, unique_for_date=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='open_id',
            field=models.UUIDField(default=uuid.uuid4(), unique=True),
            preserve_default=False,
        ),
    ]

from itertools import chain

from django.db import models

from tcshop.models import User, NameDescriptionMixin, PriorityMixin, ChoiceEnum
from categories.models import Category, CharacteristicImprint


class Product(NameDescriptionMixin):
    price = models.IntegerField()
    category = models.ForeignKey(Category, related_name="products", on_delete=models.CASCADE)
    photo_path = models.CharField(max_length=64)
    vendor_code = models.CharField(max_length=256)
    presence = models.BooleanField()
    type = models.CharField(max_length=256)

    @property
    def characteristics(self):
        return list(
            chain(
                self.bool_characteristics.all(),
                self.any_characteristics.all(),
                self.vector_characteristics.all(),
                self.scalar_characteristics.all(),
            )
        )

    def is_suitable_for_characteristics(self, params):
        matched = []
        for name, constraint in params.items():
            if name.endswith("__in"):
                characteristic = self.vector_characteristics.filter(name=name[:-4], value__in=constraint)
                matched.append(bool(characteristic))
            elif name.endswith("__is"):
                characteristic = self.bool_characteristics.filter(name=name[:-4], value=constraint)
                matched.append(bool(characteristic))
            elif name.endswith("__gt"):
                characteristic = self.scalar_characteristics.filter(name=name[:-4], value__gt=constraint)
                matched.append(bool(characteristic))
            elif name.endswith("__lt"):
                characteristic = self.scalar_characteristics.filter(name=name[:-4], value__lt=constraint)
                matched.append(bool(characteristic))
        return all(matched)


class Characteristic(models.Model):
    name = models.CharField(max_length=256)
    product = models.ForeignKey(Product, related_name="%(class)s_characteristics", on_delete=models.CASCADE)
    imprint = models.ForeignKey(CharacteristicImprint, related_name="%(class)s", on_delete=models.CASCADE)

    class Meta:
        abstract = True

    @classmethod
    def create_with_type(cls, name, product, imprint, type, value):
        if type == CharacteristicImprint.Types.BOOL.value:
            Bool.objects.create(name=name, product=product, imprint=imprint, value=True if value == "Есть" else False)
        elif type == CharacteristicImprint.Types.SCALAR.value:
            Scalar.objects.create(name=name, product=product, imprint=imprint, value=int(value))
        elif type == CharacteristicImprint.Types.VECTOR.value:
            Vector.objects.create(name=name, product=product, imprint=imprint, value=value)
        else:
            Any.objects.create(name=name, product=product, imprint=imprint, value=value)


class Bool(Characteristic):
    value = models.BooleanField()


class Any(Characteristic):
    value = models.CharField(max_length=64)


class Scalar(Characteristic):
    value = models.IntegerField()


class Vector(Characteristic):
    value = models.CharField(max_length=256)


class Order(models.Model):
    class Statuses(ChoiceEnum):
        NEW = "New"
        PROCESSED = "PROCESSED"
        SHIPPED = "Shipped"

    created_at = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=64, choices=Statuses.choices(), default=Statuses.NEW)
    total = models.IntegerField()
    user = models.ForeignKey(User, related_name="purchases", on_delete=models.SET_NULL, blank=True, null=True)
    number = models.IntegerField()
    open_id = models.UUIDField(unique=True)


class PurchasedProduct(models.Model):
    product = models.ForeignKey(Product, related_name="purchased", on_delete=models.CASCADE)
    name = models.CharField(max_length=256)
    price = models.IntegerField()
    order = models.ForeignKey(Order, related_name="items", on_delete=models.CASCADE)

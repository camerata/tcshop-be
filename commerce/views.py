import uuid
import datetime
import jwt

from django_filters.rest_framework import DjangoFilterBackend
from django.core.cache import cache
from rest_framework.decorators import list_route
from rest_framework.filters import SearchFilter, OrderingFilter

from rest_framework.response import Response

from commerce.models import Product, Category, Order, PurchasedProduct
from commerce.permissions import ReadOnly, AllowAnonymous, IsAdminOrModerator
from commerce.serializers import ProductSerializer, OrderSerializer

from rest_framework import permissions, viewsets


def sanitize_json(data):
    result = {}
    for key, value in data.lists():
        if key.endswith("__in"):
            result.update({key: value})
        elif value[0].isdigit():
            result.update({key: int(value[0])})
        elif value[0].lower() == "true":
            result.update({key: True})
        elif value[0].lower() == "false":
            result.update({key: False})
        else:
            result.update({key: value[0]})
    return result


class ProductViewSet(viewsets.ModelViewSet):

    serializer_class = ProductSerializer
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        ReadOnly,
    )

    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)
    search_fields = ('name', 'vendor_code')
    ordering_fields = ('name', 'price')

    class Meta:
        ordering = ('id',)

    def get_queryset(self):
        query = sanitize_json(self.request.GET)
        category_name = query.pop('category', None)
        if category_name is not None:
            category = Category.get_by_identifier(category_name)
            products_in_category = category.products.all()
            matched_ids = [product.id for product in products_in_category if product.is_suitable_for_characteristics(query)]
            queryset = Product.objects.filter(pk__in=matched_ids)
        else:
            queryset = Product.objects.all()

        return queryset


class OrderViewSet(viewsets.ViewSet):

    permission_classes = (AllowAnonymous, )

    def list(self, request):
        open_orders = [{key: cache.get(key)} for key in cache.keys("*")]
        checkouted_orders = Order.objects.all()
        return Response({
            "open": open_orders,
            "checkout": OrderSerializer(checkouted_orders, many=True).data
        })

    def retrieve(self, request, pk=None):
        token = request.GET.get("token", None)
        decoded = jwt.decode(token, 'secret', algorithms=['HS256'])
        if decoded["number"] != pk:
            return Response("Malicious operation", status=400)
        else:
            identifier = decoded["open_id"]
            data = cache.get(identifier)
            ordered = Product.objects.filter(pk__in=data["items"])
            return Response({"status": "OK", "number": pk, "token": token, "payload": ProductSerializer(ordered, many=True).data})

    def create(self, request):
        """Create new open order"""
        identifier = uuid.uuid4()
        data = request.POST
        current_time = datetime.datetime.now()
        number = str(current_time.day) + str(current_time.hour) + str(identifier.int % 1000)
        cache.set(identifier, {
            "number": number,
            "items": data,
            "updated_at":  current_time.strftime('%y.%m.%d %H:%M:%S')
        }, timeout=60*60*24*200)
        encoded = jwt.encode({
            "open_id": str(identifier),
            "number": number,
            "updated_at": current_time.strftime('%y.%m.%d %H:%M:%S')
        }, 'secret', algorithm='HS256')
        return Response({"status": "OK", "token": encoded, "number": number})

    def partial_update(self, request, pk=None):
        """Update items in open order"""
        token = request.data.get("token", None)
        payload = request.data.get("payload", [])
        decoded = jwt.decode(token, 'secret', algorithms=['HS256'])
        if decoded["number"] != pk:
            return Response("Malicious operation", status=400)
        else:
            identifier = decoded["open_id"]
            current_time = datetime.datetime.now()
            cache.delete(identifier)
            cache.set(identifier, {
                "number": decoded["number"],
                "items": payload,
                "updated_at": current_time.strftime('%y.%m.%d %H:%M:%S')
            }, timeout=60*60*24*200)
            encoded = jwt.encode({
                "open_id": identifier,
                "number": pk,
                "updated_at": current_time.strftime('%y.%m.%d %H:%M:%S')
            }, 'secret', algorithm='HS256')
            return Response({"status": "OK", "token": encoded, "number": pk})

    def destroy(self, request, pk=None):
        """Destroy open order"""
        token = request.GET.get("token", None)
        decoded = jwt.decode(token, 'secret', algorithms=['HS256'])
        if decoded["number"] != pk:
            return Response("Malicious operation", status=400)
        else:
            identifier = decoded["open_id"]
            cache.delete(identifier)
            return Response({"status": "OK", "number": pk})

    def update(self, request, pk=None):
        """Process an order and save it to database"""
        token = request.data.get("token", None)
        decoded = jwt.decode(token, 'secret', algorithms=['HS256'])
        if decoded["number"] != pk:
            return Response("Malicious operation", status=400)
        else:
            identifier = decoded["open_id"]
            data = cache.get(identifier)
            cache.delete(identifier)
            if request.user.is_authenticated:
                user = request.user
            else:
                user = None
            order = Order.objects.create(
                user=user,
                number=int(pk),
                open_id=identifier,
                total=0
            )
            for id in data["items"]:
                print("=" * 80)
                purchase = Product.objects.get(pk=id)
                PurchasedProduct.objects.create(
                    product=purchase,
                    price=purchase.price,
                    name=purchase.name,
                    order=order
                )
                order.total += purchase.price
            order.save()

            return Response({"status": "OK", "number": pk})


from django.core.management.base import BaseCommand, CommandError
from django.db.transaction import atomic

from categories.models import Imprint
from commerce.models import Product, Category, Characteristic

import pandas as pd
import yaml


class Command(BaseCommand):
    help = 'Loads products from csv into database'

    GENERAL_INFO = {
        "title", "picture", "price", "vendor_code", "description", "presence", "type"
    }

    def add_arguments(self, parser):
        parser.add_argument('filepath', type=str)

        # Named (optional) arguments
        parser.add_argument(
            '--truncate',
            '-t',
            action='store_true',
            dest='truncate',
            help='Truncate previous records',
        )

        parser.add_argument(
            '--skip-errors',
            action='store_true',
            dest='skip',
            help='Skip errors instead of aborting transaction',
        )

        parser.add_argument(
            '--create-categories',
            action='store_true',
            dest='create',
            help='Create categories that do not exist',
        )

    def _create_product(self, data, options):

        name = data[self.mappings["title"]]
        photo = data[self.mappings["picture"]]
        price = data[self.mappings["price"]]
        vendor_code = data[self.mappings["vendor_code"]]
        description = data[self.mappings["description"]]
        presence = data[self.mappings["presence"]]
        _type = data[self.mappings["type"]]

        try:
            if pd.isna(_type):
                self.stderr.write("Inappropriate data given: Have a product {} with no type".format(name))
                raise ValueError("No type given")
            else:
                category = Category.get_by_name(_type)
        except KeyError as e:
            self.stderr.write("Inappropriate data given: Have a product {} with no category".format(name))
            raise e
        except Category.DoesNotExist as e:
            self.stderr.write("Couldn't find category: Have a product {} with category set to {}".format(name, _type))
            raise e

        if pd.isna(price):
            self.stderr.write("Inappropriate data given: Have a product {} with no price".format(name))
            raise ValueError("No price given")

        new_product = Product.objects.create(
            name=name,
            photo_path=photo,
            price=int(price.replace(",", "")) if not isinstance(price, int) else price,
            vendor_code=vendor_code,
            description=description,
            presence=True if presence == "Есть" else False,
            category=category,
            type=_type
        )

        try:
            for group in category.imprint.characteristics_group.all():
                try:
                    for characteristic in group.characteristics.all():
                        if characteristic.name not in self.mappings["characteristics"] or self.mappings["characteristics"][characteristic.name] == "???":
                            self.stderr.write("Mapping error: No columns specified for characteristic `{}`".format(
                                characteristic.name))
                            continue

                        columns = data[self.mappings["characteristics"][characteristic.name]]

                        if isinstance(columns, pd.DataFrame) or isinstance(columns, pd.Series):
                            value = columns.dropna().values
                            if not value:
                                raise ValueError("No value: No value given for the `{}` property on `{}`".format(characteristic.name, name))
                            elif len(value) > 1:
                                raise ValueError("Ambiguous column: Two or more columns provide `{}` for `{}`".format(characteristic.name, name))
                            else:
                                value = value.item(0)
                        else:
                            value = columns

                        print(characteristic.name, value)
                        Characteristic.create_with_type(
                            name=characteristic.name,
                            imprint=characteristic,
                            type=characteristic.type,
                            product=new_product,
                            value=value
                        )
                except KeyError as e:
                    if options['skip']:
                        continue
                    else:
                        raise e
        except Imprint.DoesNotExist:
            self.stderr.write("No Imprint Error: Imprint doesn't exist for category `{}`".format(category.name))

    def handle(self, *args, **options):

        data = pd.read_csv(options['filepath'], sep=";")
        self.mappings = yaml.load(open('csv_mappings.yml'))["product"]

        if options['truncate']:
            Product.objects.all().delete()
            self.stdout.write("Previous records were successfully deleted")

        with atomic():
            for index, row in data.iterrows():
                try:
                    self._create_product(row, options)
                    self.stdout.write("Product {} with with category {} created".format(
                        row[self.mappings["title"]],
                        row[self.mappings["type"]]
                    ))
                except Exception as e:
                    if options['skip']:
                        self.stderr.write(
                            "Exception: `{}` occurred while processing row number {}. Continuing execution.".format(
                                e,
                                index
                            )
                        )
                        continue
                    else:
                        raise e
        self.stdout.write(self.style.SUCCESS('Successfully migrated csv into database'))